In order to run this you need to create three folders:

```sh
mkdir HLF LLF merged
```

Customization options are avaliable in file  **preprocessing.py**

- Select the data source

```py
file_path = r'../../data/events_anomalydetection.h5' 
```

- Choose how to distribute the workload
```py
first_event_idx = 0 
last_event_idx = 1_100_000
chunksize = 10_000 
n_cores = 11
```

- Clustering and feature extraction options

```py
task.cluster_algo = 'antikt'
task.R = 1.
task.ptmin = 20
task.njets = 2
task.Rsub = 0.1
task.ptmin2 = 0
task.dcut = 0.1
task.folderHLF = './HLF/'
task.folderLLF = './LLF/'
task.maxCnstsDump = 10
task.no_rings = 10 
task.ring_maxR = 1.
```

You can run the preprocessing algorithm by using:
```sh
python preprocessing.py
```

And finally merge the data with:
```sh
python merge.py
```