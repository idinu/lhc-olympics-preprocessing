import shutil
import os
import glob
import argparse

import pandas as pd

from jobmonitor import JobMonitor


masterkey = None#r'../data/events_LHCO2020_BlackBox1.masterkey'

#file_path = r'../data/events_LHCO2020_BlackBox1.h5' 
#file_path = r'../data/events_anomalydetection_Z_XY_qqq.h5' 
#file_path = r'..data/events_LHCO2020_backgroundMC_Pythia.h5'
file_path = r'../data/events_anomalydetection.h5'

''' Distribution of events among cores '''

first_event_idx = 0 
last_event_idx = 1_100_000
chunksize = 1_100 
n_cores = 1

def merge(path):
    '''
    Merge all .hdf files in given directory

    Returns:
      DataFrame from merged .hdf files
    '''
    signal_files = sorted(glob.glob(path + "signal*"))
    background_files = sorted(glob.glob(path + "background*"))
    
    files_list = [background_files, signal_files]
    dfs_merged = []
    
    for files in files_list:
        df_list = []
        if files:
            for filename in files:
                df = pd.read_hdf(filename)
                df.reset_index(drop=True)
                df_list.append(df)
        dfs_merged.append(pd.concat(df_list, axis=0, ignore_index=True)
                          if df_list else None)
       
    return dfs_merged[0], dfs_merged[1]


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--merge', action='store_true')

    args = parser.parse_args()
    
    if not args.merge:
        # Create a job monitor instance for paralell clustering of events
        task = JobMonitor(file_path, 
                          first_event_idx, 
                          last_event_idx, 
                          chunksize, 
                          n_cores)

        ''' Preprocessing parameters (these are their default values) '''
        task.cluster_algo = 'antikt'
        task.R = 1.
        task.ptmin = 0
        task.njets = 3
        task.Rsub = 0.2
        task.ptmin2 = 0
        task.dcut = 0.1
        task.folderHLF = './HLF/'
        task.folderLLF = './LLF/'
        task.maxCnstsDump = 200
        task.rings = [[0, 0.01], [0.01, 0.01668101], [0.01668101, 0.02782559], 
                      [0.02782559, 0.04641589], [0.04641589, 0.07742637], 
                      [0.07742637, 0.12915497], [0.12915497, 0.21544347], 
                      [0.21544347, 0.35938137], [0.35938137, 0.59948425], 
                      [0.59948425, 1]]
        task.masterkey = masterkey

        print('Initializing . . .')
        folders = (task.folderHLF[:-1], task.folderLLF[:-1])
        for folder in folders:
            if os.path.exists(folder):
                shutil.rmtree(folder)
            os.mkdir(folder)

        print('Starting . . .')
        task.defineJobs()
        task.startJobs()

    print('Merging . . .')
    
    feature_types = ['HLF']#, 'LLF']
    feature_folders = ['./HLF/']#,'./LLF/']

    save_folder = './merged/'
    output_file_suffix = '_merged.h5'

    for features, folder in zip(feature_types, feature_folders):
        bkg_df, sig_df = merge(folder)
        if bkg_df is not None:
            bkg_df.to_hdf(save_folder+'bkg'+features+output_file_suffix, key = 'bkg')
        if sig_df is not None:
            sig_df.to_hdf(save_folder+'sig'+features+output_file_suffix, key = 'sig')

    
    # from payload import Payload

    # p1 = Payload(file_path,first_event_idx,chunksize,njets=2,rings=task.rings)

    # p1.clustering()
    # p1.HLFextraction()
    # p1.LLFdump(20, True)
    # print(p1.LLF['signal'])
    # p1.saveLLF('./')
    

