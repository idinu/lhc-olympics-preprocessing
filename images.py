import os
import glob
import shutil
import argparse
import json
import re
from itertools import chain

import pandas as pd
import numpy as np
import h5py
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from tqdm import tqdm
from pyjet import cluster, DTYPE_PTEPM, PseudoJet

def pixelate(jet, 
             npix=32, 
             img_width=0.8, 
             trim=False,
             norm=False, 
             avg_centroid=False,
             rotate=True,
             **kwargs):
    """A function for creating a jet image from an array of particles.
    **Arguments**
    - **jet** : _numpy.ndarray_
        - An array of particles where each particle is of the form 
        `[pt,y,phi]` 
    - **npix** : _int_
        - The number of pixels on one edge of the jet image, which is
        taken to be a square.
    - **img_width** : _float_
        - The size of one edge of the jet image in the rapidity-azimuth
        plane.
    - **trim** : _bool_
        - Wheter to apply jet trimming based on options
    - **avg_centroid**: _bool_
        - Center the image based on the average pT centroid
    - **rotate** : _bool_
        - Rotate image to align leading secondary clusters on the vertical
    - **norm** : _bool_
        - Whether to normalize the $p_T$ pixels to sum to `1`.
    **Returns**
    - _3-d numpy.ndarray_
        - The jet image as a `(npix, npix, 1)` array. Note that the order
        of the channels changed in version 1.0.3.
    """

    # Read **kwargs and overwrite any default option values
    options = {
        # fraction of main cluster pT required for subjet to pass trimming
        'fcut': 0.05,   
        # radius of the main jet cluster
        'R1': 1.0,      
        # radius of the secondary clusters
        'R2': 0.2,    
        # pivot offset from bottom axis, relative to the image width  
        'offset': 0.5,  
    }
    options.update(kwargs)

    # set columns
    (pT_i, rap_i, phi_i) = (0, 1, 2)

    # the image is (img_width x img_width) in size
    pix_width = img_width / npix
    jet_image = np.zeros((npix, npix, 1))

    if trim or not avg_centroid:
        # reclustering to find subjets
        pseudojets_input = np.zeros(jet.shape[0], dtype=DTYPE_PTEPM)
        pseudojets_input['pT'] = jet[:,pT_i]
        pseudojets_input['eta'] = jet[:,rap_i]
        pseudojets_input['phi'] = jet[:,phi_i]
        sequence = cluster(pseudojets_input, R=options['R2'], p=1)
        subjet_array = sequence.inclusive_jets()

    if trim:
        trimmed_jet = []
        for subjet in subjet_array:
            # get main cluster pt
            main_cluster = cluster(pseudojets_input, R=options['R1'], p=-1)
            parent_pt = main_cluster.inclusive_jets()[0].pt

            if subjet.pt > parent_pt * options['fcut']:
                # Get the subjets pt, eta, phi constituents
                subjet_data = subjet.constituents_array()
                subjet_data = subjet_data.view((float, 
                                len(subjet_data.dtype.names))
                            )
                trimmed_jet.append(subjet_data)
        jet = np.concatenate(trimmed_jet)[:,:3]

    # remove particles with zero pt
    jet = jet[jet[:,pT_i] > 0]

    if not avg_centroid:
        # define pivot and edge coordinates
        rap_p = subjet_array[0].eta
        phi_p = subjet_array[0].phi
        if len(subjet_array) < 2:
            rotate = False
        else:
            rap_e = subjet_array[1].eta
            phi_e = subjet_array[1].phi
        new_origin = np.array([0, rap_p, phi_p])
        
        # translate jet to put pivot in origin (0, 0)
        jet = jet-new_origin
        
        # clusters = np.array([[sj.pt, sj.eta, sj.phi] for sj in subjet_array])
        # clusters_image = np.zeros((npix, npix, 1))

        if rotate:
            # compute the unit vector of the edge
            r = np.array([rap_p-rap_e, phi_p-phi_e])
            ur = r/np.linalg.norm(r)

            # define vertical axis unit vector 
            uy = np.array([0, 1])

            # rotation angle for bringing edge on the certical axis
            theta = np.arccos(np.dot(uy, ur))*(np.sign(ur[0]))
            
            # anticlockwise rotation matrix
            rotate2d = np.array([[np.cos(theta), np.sin(theta)],
                                [-np.sin(theta), np.cos(theta)]])

            # apply rotation to all jet constituents' coordinates
            jet[:,1:] = np.dot(jet[:,1:],rotate2d)

        # translate to place pivot according to offset
        jet = jet+np.array([0, img_width/2, img_width*options['offset']])

        # transistion to indices 
        rap_indices = np.ceil(jet[:,rap_i]/pix_width - 0.5) 
        phi_indices = np.ceil(jet[:,phi_i]/pix_width - 0.5)

    else:
        if rotate:
            raise NotImplementedError("Rotation only available when avg_centroid=False")
        # average centroid and transistion to indices
        rap_avg = np.average(jet[:,rap_i], weights=jet[:,pT_i])
        phi_avg = np.average(jet[:,phi_i], weights=jet[:,pT_i])
        rap_pt_cent_index = np.ceil(rap_avg/pix_width - 0.5) - np.floor(npix / 2)
        phi_pt_cent_index = np.ceil(phi_avg/pix_width - 0.5) - np.floor(npix / 2)
        rap_indices = np.ceil(jet[:,rap_i]/pix_width - 0.5) - rap_pt_cent_index
        phi_indices = np.ceil(jet[:,phi_i]/pix_width - 0.5) - phi_pt_cent_index

    # delete elements outside of range
    mask = np.ones(jet[:,rap_i].shape).astype(bool)
    mask[rap_indices < 0] = False
    mask[phi_indices < 0] = False
    mask[rap_indices >= npix] = False
    mask[phi_indices >= npix] = False
    rap_indices = rap_indices[mask].astype(int)
    phi_indices = phi_indices[mask].astype(int)

    # construct grayscale image
    for pt,y,phi in zip(jet[:,pT_i][mask], rap_indices, phi_indices): 
        jet_image[phi, y, 0] += pt

    # L1-normalize the pt channels of the jet image
    if norm:
        normfactor = np.sum(jet_image[...,pT_i])
        if normfactor == 0:
            raise FloatingPointError('Image had no particles!')
        else: 
            jet_image[...,0] /= normfactor

    return jet_image 

def image_from_row(row, stitch_jets=False, **kwargs):
    njets = len(row)
    if stitch_jets:
        arr = np.concatenate([x for x in row if x is not None])
        img = np.asarray(pixelate(arr, **kwargs))
    else:
        arr = [pixelate(x, **kwargs) for x in row if x is not None]
        arr = np.squeeze(np.asarray(arr),-1)
        img = np.moveaxis(arr, 0, -1)
        missing_jets = njets - img.shape[2]
        npix = img.shape[0]
        if missing_jets>0:
            padding = np.zeros(shape=(npix,npix,missing_jets))
            img = np.concatenate([img, padding], axis=2)
    return img

if __name__ == "__main__":


    parser = argparse.ArgumentParser()
    parser.add_argument('--outDir', action='store', type=str, default='./images/')
    parser.add_argument('--inDir', action='store', type=str, default='./LLF/')
    parser.add_argument('--multijet', action='store', type=bool, default=True)
    parser.add_argument('--npix', action='store', type=int, default=32)
    parser.add_argument('--width', action='store', type=float, default=8.9)
    parser.add_argument('--rotate', action='store', type=bool, default=True)

    args = parser.parse_args()

    if os.path.exists(args.outDir):
        shutil.rmtree(args.outDir)
    os.mkdir(args.outDir)

    output_file_suffix = '_imgs.h5'
    input_files = sorted(glob.glob(f'{args.inDir}*LLF*'))
    
    img_def = {
        "npix": 64,
        "img_width": 1.6,
        "rotate": True,
        "offset": 0.66,
        "norm": True,
        "stitch_jets": False
    }

    imgs_bkg = []
    imgs_sig = []
    for i, file in enumerate(input_files):
        print(f"({i+1}/{len(input_files)}) Processing file {file}. . .") 

        llf = pd.read_hdf(file).to_numpy()
        #print(np.array([image_from_row(row, **img_def) for row in tqdm(llf)]).shape)
        if "signal" in file:
            imgs_sig.append(np.array([image_from_row(row, **img_def) for row in tqdm(llf)], dtype=np.float32))
        else:    
            imgs_bkg.append(np.array([image_from_row(row, **img_def) for row in tqdm(llf)], dtype=np.float32))

        #prefix = os.path.split(file)[-1].split('_')[0]
        #print(prefix)
        
        
        #imgs_file = args.outDir + prefix + output_file_suffix
    d = {"sig":imgs_sig, "bkg":imgs_bkg}
    for k, v in d.items(): 
        imgs_file = args.outDir + k + output_file_suffix
        imgs = np.concatenate(v)
        hf = h5py.File(imgs_file, 'w')
        hf.create_dataset('multijet', data=imgs)
        hf.close()

        eps = 1e-6

        if img_def['stitch_jets']:
            avg = np.mean(imgs,axis=0)
            plt.figure(figsize = (10,10))
            plt.imshow(avg+eps, origin='lower', norm=LogNorm())
            plt.savefig(args.outDir + k +'_avg.jpg')
        else:
            for i in range(imgs.shape[3]):
                avg = np.mean(imgs[:,:,:,i],axis=0)
                plt.figure(figsize = (10,10))
                plt.imshow(avg+eps, origin='lower', norm=LogNorm())
                plt.savefig(args.outDir + k + f"{i+1}" + '_avg.jpg')

        
    

   
