import multiprocessing as mpi
import time
import os

import numpy as np

from payload import Payload


def jobDriver(file_path, first_event_idx, last_event_idx, masterkey, pid, 
              comm_link, folderHLF, folderLLF, maxCnstsDump, cluster_algo, R, 
              ptmin, njets, Rsub, ptmin2, dcut, rings):

    job = Payload(file_path, first_event_idx, last_event_idx, masterkey, 
                  comm_link, pid, ptmin, njets, cluster_algo, R, Rsub, ptmin2, 
                  dcut, rings)
    
    job.clustering()
    job.HLFextraction()
    job.LLFdump(maxCnstsDump)
    job.saveHLF(folderHLF)
    job.saveLLF(folderLLF)
    job.terminate()

class JobMonitor:
    def __init__(self, file_path, first_event_idx, last_event_idx, 
                 chunksize, n_cores):
        
        ''' Monitor parameters '''
        self.tBegin = time.time()
        self.file_path = file_path
        self.chunksize = chunksize
        self.n_events = last_event_idx-first_event_idx
        self.n_cores = n_cores
        self.n_procs = int(self.n_events/chunksize)
        self.n_procs_done = 0
        self.n_procs_working = 0
        self.chunk_start = [first_event_idx + i*chunksize 
                            for i 
                            in range(self.n_procs)]
        self.chunk_stop = [first_event_idx + (i+1)*chunksize 
                           for i 
                           in range(self.n_procs)]
        self.procStarted = np.repeat(False, self.n_procs)
        self.procWorking = np.repeat(False, self.n_procs)
        self.procFinished = np.repeat(False, self.n_procs)
        
        ''' Display parameters '''
        self.state = {1:'CLUSTERING Background',2:'CLUSTERING SIGNAL', 
                      3:'FEATURE EXTRACTION Background', 
                      4:'FEATURE EXTRACTION Signal',
                      5:'LLF DUMP Background', 6:'LLF DUMP Signal',
                      10: 'Saving Filles . . .', 69:'Core IDLE'}
        self.bar_length = 20
        self.dbg = False
                
        ''' Default preprocessing parameters '''
        self.cluster_algo = 'antikt'
        self.R = 1.
        self.ptmin = 20
        self.njets = 2
        self.Rsub = 0.1
        self.ptmin2 = 0
        self.dcut = 0.1
        self.folderHLF = './HLF/'
        self.folderLLF = './LLF/'
        self.maxCnstsDump = 10
        self.rings = None
        self.masterkey = None

    def defineJobs(self):
        pipes = [mpi.Pipe() for i in range(self.n_procs)]
        self.terminal, self.link = list(map(list, zip(*pipes)))

        self.procs = [mpi.Process(target=jobDriver, 
                      args=(self.file_path, self.chunk_start[i], 
                            self.chunk_stop[i], self.masterkey, i, 
                            self.link[i], self.folderHLF, 
                            self.folderLLF, self.maxCnstsDump, 
                            self.cluster_algo, self.R, self.ptmin, self.njets, 
                            self.Rsub, self.ptmin2, self.dcut, self.rings,
                            )
                        )
                     for i 
                     in range(self.n_procs)]
    
    def startJobs(self):
        while(self.n_procs_done != self.n_procs): 
            self.n_procs_done = np.count_nonzero(self.procFinished)
            startedCount = np.count_nonzero(self.procStarted)
            for _ in range(self.n_cores-self.n_procs_working):
                if startedCount < self.n_procs:
                    j = np.argmax(self.procStarted == False)
                    self.procs[j].start()
                    self.procWorking[j] = True
                    self.procStarted[j] = True
            
            self.general_state = []
            for w in np.where(self.procWorking==True)[0]:
                if self.procFinished[w] == True: 
                    self.procs[w].join()
                    self.procWorking[w] = False
                    continue
                package = self.terminal[w].recv()
                if package==(69,0):
                    self.procWorking[w] = False
                    self.procs[w].join()
                self.general_state += [package]

            for s in np.where(self.procStarted==True)[0]: 
                if not self.procs[s].is_alive(): 
                    self.procFinished[s] = True   
            self.n_procs_working = np.count_nonzero(self.procWorking)   
            self.__stdout()  
    
    def __stdout (self):
        prcFin = self.__general_progress() 
        block = int(round(2* self.bar_length * prcFin))
        pbar = "[{0}] {1:.2f}%".format( "#" * block + "-" * (self.bar_length*2 - block), prcFin * 100)
        dt = time.time() - self.tBegin
        m = int(dt //60)
        h = int(m//60)
        m = m%60
        s = int(dt)%60
        os.system('clear')
        #os.system('clear && printf "\e[3J"')
        print('PARALELL FEATURE EXTRACTION')
        print(30*'<>')
        print(f'Processing {self.n_events:d} events in chunks of {self.chunksize:d} using {self.n_cores:d} CPU cores')
        print(30*'<>')
        print(f'Finished Chunks:{self.n_procs_done:d} / {self.n_procs:d}')
        print(pbar)
        print()
        print(60*'=')
        if(prcFin != 1):
            for i in range(self.n_procs_working):
                print(f'Core: {i+1:d}')
                self.__update_progress(self.general_state[i])
                print(60*'-')
            print(f'Time Elapsed - {h:02d}h:{m:02d}m:{s:02d}s')
        else: 
            print('ALL JOBS DONE')
            print(60*'-')
            print(f'Time Elapsed - {h:02d}h:{m:02d}m:{s:02d}s')
        print(60*'=')
        if self.dbg:
            print(f'S:{self.procStarted}')
            print(f'W:{self.procWorking}')
            print(f'F:{self.procFinished}')
            print(f'working:{self.n_procs_working} / {self.n_cores}')
    
    def __update_progress(self, proc_state):
        progress = proc_state[1]
        if isinstance(progress, int):
            progress = float(progress)
        if not isinstance(progress, float):
            progress = 0
        if progress < 0:
            progress = 0
        if progress >= 1:
            progress = 1

        block = int(round(self.bar_length * progress))

        print(self.state[proc_state[0]])
        text = "Progress: [{0}] {1:.2f}%".format( "#" * block + "-" * (self.bar_length - block), progress * 100)
        print(text)
        
    def __general_progress(self):
        prc = self.n_procs_done/self.n_procs
        div = len(self.state) - 2
        if prc == 1: return prc
        partialProgress = 0
        for coreProgress in self.general_state:
            s = coreProgress[0]
            p = coreProgress[1]
            partialProgress += (1/self.n_procs)*(s/div+(p-1)/div) 
        return prc+partialProgress

    
