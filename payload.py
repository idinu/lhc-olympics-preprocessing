import numpy as np
import pandas as pd

from energyflow.utils import pixelate
from itertools import chain
from scipy.ndimage import rotate

from pyjet import cluster, DTYPE_PTEPM, JetDefinition, PseudoJet

JET_FEATURES = ['pt', 'eta', 'phi', 'E', 'm', 'nc', 'nisj', 'nesj', 
                '1tau', '2tau', '3tau', '32tau', '21tau']
EVENT_FEATURES = ['mjj', 'nj', 'mjjj']

class EventClustered:
    def __init__(self, raw_event, cluster_algo, R, ptmin_cluster):

        n_particles = raw_event.shape[0]//3
        pseudojets_input = np.zeros(len([x for x in raw_event[::3] if x > 0]),
                                    dtype=DTYPE_PTEPM)
        jdef = JetDefinition(cluster_algo, R)
        for i in range(n_particles):
            if (raw_event[i*3]>0):
                pseudojets_input[i]['pT'] = raw_event[i*3]
                pseudojets_input[i]['eta'] = raw_event[i*3+1]
                pseudojets_input[i]['phi'] = raw_event[i*3+2]
        sequence = cluster(pseudojets_input, jdef)
        self.jets = sequence.inclusive_jets(ptmin=20)
    
    def __repr__(self):
        return str(self.jets)
         

class Payload:
    def __init__(self, file_path, first_event_idx, last_event_idx, 
                 masterkey=None, comm_link=None, pid=None, ptmin=20, njets=2, 
                 cluster_algo='antikt', R=1., Rsub=0.2, ptmin2=0, dcut=0.1, 
                 rings=None):

        raw_data_all = pd.read_hdf(file_path, start=first_event_idx, 
                                   stop=last_event_idx)
        ncols = raw_data_all.shape[1]
        self.raw_data = {}
        if (ncols%3) == 1 and not masterkey:
            truth_bit = ncols-1
            signal_mask = raw_data_all[truth_bit]==1
            self.raw_data['signal'] = raw_data_all[signal_mask] \
                                            .drop(truth_bit, axis=1) \
                                            .reset_index(drop = True)
            self.raw_data['background'] = raw_data_all[~signal_mask] \
                                            .drop(truth_bit, axis=1) \
                                            .reset_index(drop = True)
        elif masterkey:
            self.masterkey = np.fromfile(masterkey, sep='\n')[first_event_idx:last_event_idx]
            signal_mask = (self.masterkey == 1.0) 
            self.raw_data['signal'] = raw_data_all[signal_mask] \
                                            .reset_index(drop = True)
            self.raw_data['background'] = raw_data_all[~signal_mask] \
                                            .reset_index(drop = True)
        elif (ncols%3) == 0:
            self.raw_data['background'] = raw_data_all \
                                            .reset_index(drop = True)
        else:
            raise ValueError("Invalid number of columns in 4-vector data")
        
        self.stage = 1
        self.comm_link = comm_link
        self.pid = pid
        self.cluster_algo = cluster_algo
        self.R = R
        self.ptmin = ptmin
        self.Rsub = Rsub
        self.dcut = dcut
        self.njets = njets
        self.ptmin2 = ptmin2
        self.rings = rings
        
        
    def clustering(self):
        self.events = {}
        self.events['background'] = self.__clusterEvents('background')
        self.stage = 2
        if 'signal' in self.events.keys():
            self.events['signal'] = self.__clusterEvents('signal')
        
    def HLFextraction(self):
        self.HLF = {}
        self.stage = 3
        self.HLF['background'] = self.__extractHLF('background')
        self.stage = 4
        if 'signal' in self.events.keys():
            self.HLF['signal'] = self.__extractHLF('signal')
        
    def LLFdump(self, stop):
        self.LLF = {}
        for key in self.events.keys():
            self.stage +=1
            n = len(self.events[key])
            eventLLF = []
            for i,event in enumerate(self.events[key]):
                jets = event.jets
                event_row = []
                if self.comm_link: self.comm_link.send((self.stage,i/n))
                for jet in jets[:self.njets]:
                    constits = jet.constituents_array()
                    event_row += [constits.view((float, 
                                        len(constits.dtype.names))
                                        )[:,:3]
                                    ]
                eventLLF += [event_row]
            self.LLF[key]=pd.DataFrame(eventLLF)

    def saveHLF(self, folder):
        for key in self.events.keys():
            if not self.HLF[key].empty:
                if self.pid is not None:
                    self.comm_link.send((10,1))
                    self.HLF[key].to_hdf(
                            folder+key+f'{self.pid:02d}'+'HLF.hdf', 
                            key=key
                        )
                else: self.HLF[key].to_hdf(folder+key+'HLF.hdf', key=key)
            
    def saveLLF(self, folder):
        for key in self.events.keys():
            if not self.LLF[key].empty:
                if self.pid is not None:
                    self.comm_link.send((10,1))
                    self.LLF[key].to_hdf(
                                folder+key+f'{self.pid:02d}'+'LLF.hdf', 
                                key=key
                            )
                else: self.LLF[key].to_hdf(folder+key+'LLF.hdf', key=key)
            
    def terminate(self):
        self.comm_link.send((69,0))
        
    def __clusterEvents(self, raw_key):
        events = []
        n_events = self.raw_data[raw_key].shape[0]
        for index, row in self.raw_data[raw_key].iterrows():
            events += [EventClustered(
                            row, 
                            self.cluster_algo, 
                            self.R, 
                            self.ptmin)
                        ]
            progress = index/n_events
            if self.comm_link: self.comm_link.send((self.stage,progress))
        return events
    
    def __extractHLF(self, event_key):
        HLF = self.__newFeatureDict()
        n = len(self.events[event_key])
        for i,event in enumerate(self.events[event_key]):
            jets = event.jets
            
            all_features = []
            all_features += [self.__extractEventFeatures(jets)]
            
            progress = (self.stage, (i+1)/n)
            if self.comm_link: self.comm_link.send(progress)
        
            for j,jet in enumerate(jets[:self.njets]):
                all_features += [self.__extractJetFeatures(jet,j)]        
                all_features += [self.__extractJetSubstructFeatures(jet,j)]
            
            if self.njets > len(jets):
                for i in range(self.njets - len(jets)):
                    all_features += [self.__nullJet(len(jets)+i)]
            
            for feature_set in all_features:
                for key in feature_set.keys():
                    HLF[key] += feature_set[key]
            
        
        progress = (self.stage, 1)
        if self.comm_link: self.comm_link.send(progress)
        return pd.DataFrame(HLF)
    
    def __extractEventFeatures(self, jets):
        event_features = {}
        E = jets[0].e + jets[1].e
        px = jets[0].px + jets[1].px 
        py = jets[0].py + jets[1].py
        pz = jets[0].pz + jets[1].pz;
        event_features['mjj'] = [(E**2-px**2-py**2-pz**2)**0.5]
        event_features['nj'] = [len(jets)]
        if len(jets) > 2:
            E += jets[2].e
            px += jets[2].px; py += jets[2].py; py += jets[2].py; 
            event_features['mjjj'] = [(E**2-px**2-py**2-pz**2)**0.5]
        else:
            event_features['mjjj'] = event_features['mjj']
        
        return event_features
        
    def __extractJetFeatures(self, jet, jet_index):
        jet_features = {}
        jet_features['pt'+str(jet_index+1)] = [jet.pt]
        jet_features['eta'+str(jet_index+1)] = [jet.eta]
        jet_features['phi'+str(jet_index+1)] = [jet.phi]
        jet_features['E'+str(jet_index+1)] = [jet.e]
        jet_features['m'+str(jet_index+1)] = [jet.mass]
        jet_features['nc'+str(jet_index+1)] = [len(jet.constituents())]
        
        return jet_features
        
       
    def __extractJetSubstructFeatures(self, jet, jet_index):
        jet_substruct_features = {}
        
        seq = cluster(jet, R=self.Rsub, algo='kt')
        cnsts = jet.constituents()
        cndts1 = seq.exclusive_jets(1)
        tau1 = self.__subjettiness(cndts1, cnsts)
        jet_substruct_features['1tau'+str(jet_index+1)] = [tau1]
        if (len(cnsts)>1):
            cndts2 = seq.exclusive_jets(2)
            tau2 = self.__subjettiness(cndts2, cnsts)
            if (len(cnsts)>2):
                cndts3 = seq.exclusive_jets(3)
                tau3 = self.__subjettiness(cndts3, cnsts)
            else: tau3 = 0
        else: 
            tau2 = 0; tau3 = 0
        
        if self.rings is not None:
            n_rings = len(self.rings) 
            eRings = self.__getEnergyRings(jet)
            for ring_idx in range (n_rings): 
                jet_substruct_features[f'eRing{ring_idx}_{jet_index+1}'] \
                    = [eRings[ring_idx]]
        
        jet_substruct_features['2tau'+str(jet_index+1)] = [tau2]
        jet_substruct_features['3tau'+str(jet_index+1)] = [tau3]
        
        jet_substruct_features['21tau'+str(jet_index+1)] = [tau2/tau1 
                                                            if tau1 != 0 
                                                            else 0]
        jet_substruct_features['32tau'+str(jet_index+1)] = [tau3/tau2 
                                                            if tau2 != 0 
                                                            else 0]
        
        jet_substruct_features['nesj'+str(jet_index+1)] = \
                                [seq.n_exclusive_jets(self.dcut)]
        jet_substruct_features['nisj'+str(jet_index+1)] = \
                                [len(seq.inclusive_jets(ptmin=self.ptmin2))]
        
        return jet_substruct_features
    
    def __newFeatureDict(self):
        feature_dictionary = {key+str(i+1):[] 
                              for i in range(self.njets) 
                              for key in JET_FEATURES}
        if self.rings is not None:
            n_rings = len(self.rings) 
            for jet_index in range(self.njets) :
                for ring_idx in range (n_rings): 
                    feature_dictionary[f'eRing{ring_idx}_{jet_index+1}'] = []
        
        for key in EVENT_FEATURES: feature_dictionary[key]=[]
        return feature_dictionary
    
    def __nullJet(self, jet_index):
        feature_dictionary = {key+str(jet_index+1):[0] for key in JET_FEATURES}
        if self.rings is not None:
            n_rings = len(self.rings) 
            for ring_idx in range (n_rings): feature_dictionary[f'eRing{ring_idx}_{jet_index+1}'] = [0]
        
        return feature_dictionary
    
    def __getEnergyRings(self, jet):
        if isinstance(self.rings, list):
            if all(isinstance(r, list) for r in self.rings):
                return [self.__energyRing(jet, ring[0], ring[1]) for ring in self.rings]
            else:
                raise ValueError("'rings' must be a nested list of intervals")
        else:
            return None 
    
    def __deltaR(self, x, y):
        return ((x.phi-y.phi)**2 + (x.eta-y.eta)**2)**0.5

    def __subjettiness(self, cndts, cnsts):
        d0 = sum(c.pt for c in cnsts)
        ls = []
        for c in cnsts:
            dRs = [self.__deltaR(c,cd) for cd in cndts]
            ls += [c.pt * min(dRs)]
        return sum(ls)/d0
    
    def __energyRing(self, jet, dR_min, dR_max):
        cnsts = jet.constituents()
        energy = 0
        for c in cnsts:
            dr = self.__deltaR(jet,c)
            if dr >= dR_min and dr <= dR_max:
                energy += c.e
        return energy/jet.e
    
    


